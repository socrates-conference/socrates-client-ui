import type { Config } from './ConfigType';

const apiEndpoint: string = 'https://api.socrates-conference.de';
const testEndpoint: string =
  'https://3j3o9g2j5b.execute-api.eu-central-1.amazonaws.com/Prod';
const config: Config = {
  conference: 'socrates24',
  logoPath: 'https://socrates-sponsors-data-logo-upload.s3.amazonaws.com',
  apiEndpoints: {
    applications: testEndpoint,
    sponsors: apiEndpoint,
    conferences: apiEndpoint,
    applicants: apiEndpoint,
    subscriptions: apiEndpoint,
  },
  isRegistrationOpen: true,
};
export default config;
