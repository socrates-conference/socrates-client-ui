// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
// noinspection JSConstantReassignment

import '@testing-library/jest-dom';

import Enzyme from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

Enzyme.configure({
  adapter: new Adapter(),
});

// @ts-ignore
if (!global.catchAll) {
  process.on('unhandledRejection', function (err) {
    console.error('Unhandled promise rejection: %o', err);
  });
  // @ts-ignore
  global.catchAll = true;
}
global.window = {
  document: {
    // @ts-ignore
    createElement: () => {},
  },
  addEventListener: () => {},
};

const localStorageMock = {
  setItem: () => {},
  getItem: () => {},
  removeItem: () => {},
};
// @ts-ignore
global.localStorage = localStorageMock;
