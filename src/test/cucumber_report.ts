const reporter = require('cucumber-html-reporter');

const version = require('../../package.json').version;

const options = {
  theme: 'bootstrap',
  jsonFile: 'coverage/cucumber_report.json',
  output: 'coverage/cucumber_report.html',
  reportSuiteAsScenarios: true,
  launchReport: true,
  metadata: {
    'App Version': version,
    'Test Environment': 'DEV',
  },
};
reporter.generate(options);

export default reporter;
