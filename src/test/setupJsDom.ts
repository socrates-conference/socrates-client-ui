// noinspection JSConstantReassignment

import './setup';
import { JSDOM } from 'jsdom';

const setup = () => {
  const jsdom = new JSDOM('<!DOCTYPE html><div id="root"></div>', {
    url: 'https://testhost.org',
  });
  jsdom.window.scrollTo = () => {};
  Object.defineProperty(global, 'jsdom', { value: jsdom, writable: true });
  Object.defineProperty(global, 'window', {
    value: jsdom.window,
    writable: true,
  });
  Object.defineProperty(global, 'document', {
    value: jsdom.window.document,
    writable: true,
  });
  Object.defineProperty(global, 'navigator', {
    value: jsdom.window.navigator,
    writable: true,
  });
  // @ts-ignore
  global.localStorage = {
    setItem: () => {},
    getItem: () => null,
    removeItem: () => {},
  };
  // @ts-ignore
  console.log(global.document.getElementById('root'));
  console.log(global.document.createEvent('Event'));
  // @ts-ignore
  return global.document.getElementById('root');
};

export default setup;
