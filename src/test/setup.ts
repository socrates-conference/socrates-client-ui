// noinspection JSConstantReassignment

import 'raf/polyfill';

// hack to make modules using exenv (such as react-modal) render correctly.
global.window = {
  document: {
    // @ts-ignore
    createElement: () => {},
  },
  addEventListener: () => {},
};

const localStorageMock = {
  setItem: () => {},
  getItem: () => {},
  removeItem: () => {},
};
// @ts-ignore
global.localStorage = localStorageMock;
