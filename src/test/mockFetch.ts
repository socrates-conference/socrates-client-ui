import 'isomorphic-fetch';
import Route from 'route-parser';

const UNCHANGED = 'UNCHANGED';
let originalFetch: any;
const DEFAULT_BASE_URL = 'http://localhost:8080/';
export default class MockFetch {
  static _defaultMockData = {
    path: {
      some: 'data',
    },
  };
  static _mockData = {};
  static _baseUrls = [DEFAULT_BASE_URL];

  static registerBaseUrl(baseUrl: string) {
    MockFetch._baseUrls.push(baseUrl);
  }

  static mock(additionalMockData: any = {}): void {
    MockFetch._mockData = {
      ...MockFetch._defaultMockData,
      ...MockFetch._mockData,
      ...additionalMockData,
    };

    MockFetch._mock((url: any) => {
      return Promise.resolve({
        headers: new Headers(),
        status: 200,
        json: () => MockFetch._getMockData(MockFetch._mockData, url),
      });
    });
  }

  static _extractPath(url: string): string {
    return MockFetch._baseUrls
      .reduce((path, baseUrl) => path.replace(baseUrl, ''), url)
      .replace(/^\/+/g, '');
  }

  static _getMockData(
    mockData: Record<string, any>,
    url: string,
  ): Record<string, any> {
    const path = MockFetch._extractPath(url);

    const paths = Object.keys(mockData);
    const foundMockData = paths
      .map(MockFetch._pathToMockData(mockData, path))
      .find((md) => md !== undefined);

    if (foundMockData === undefined) {
      throw new Error(`Could not find mock data for ${url}.`);
    }

    return foundMockData;
  }

  static _pathToMockData(mockData: Record<string, any>, url: string) {
    return (path: string): Record<string, any> | void => {
      const route = new Route(path);
      const match = route.match(url);

      if (match) {
        if (typeof mockData[path] === 'function') {
          return mockData[path](match);
        }

        return mockData[path];
      }

      return undefined;
    };
  }

  static mockUnchanged(): void {
    MockFetch._mock(() => {
      return Promise.resolve({
        headers: new Headers(),
        status: 304,
        json: () => UNCHANGED,
      });
    });
  }

  static mockUnauthorized(): void {
    MockFetch._mock(() => {
      return Promise.resolve({
        headers: new Headers(),
        status: 401,
        json: undefined,
      });
    });
  }

  static mockServerError(): void {
    MockFetch._mock(() => {
      return Promise.resolve({
        headers: new Headers(),
        status: 500,
        json: () => undefined,
      });
    });
  }

  static mockInvalid(): void {
    MockFetch._mock(() => {
      return Promise.resolve({
        headers: new Headers(),
        status: 200,
        json: () => 'invalid',
      });
    });
  }

  static _mock(fn: any): void {
    if (!originalFetch) {
      originalFetch = global.fetch;
    }

    global.fetch = fn;
  }

  static reset(): void {
    if (originalFetch) {
      global.fetch = originalFetch;
      originalFetch = undefined;
    }

    MockFetch._mockData = {};
    MockFetch._baseUrls = [DEFAULT_BASE_URL];
  }
}
