import React from 'react';
import { shallow } from 'enzyme';
import Imprint from './Imprint';
describe('(Component) Format', () => {
  const wrapper = shallow(<Imprint />);
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
