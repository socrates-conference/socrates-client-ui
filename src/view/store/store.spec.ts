import rootSaga from '../sagas/rootSaga';
import StoreFactory, { HistoryFactory } from './store';
import rootReducer from '../reducers/rootReducer';
describe('store', () => {
  let store: ReturnType<typeof StoreFactory.createStore>;
  beforeEach(() => {
    store = StoreFactory.createStore(
      rootReducer,
      rootSaga,
      HistoryFactory.createTestHistory(),
    );
  });
  it('can be created', () => {
    expect(store).toBeDefined();
  });
});
