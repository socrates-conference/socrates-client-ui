import React from 'react';
import { shallow } from 'enzyme';
import PrivacyPolicy from './PrivacyPolicy';
describe('(Component) Home', () => {
  const wrapper = shallow(<PrivacyPolicy />);
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
