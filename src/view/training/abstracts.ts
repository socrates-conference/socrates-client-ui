export const abstracts = {
  tdd_foundations: {
    author: `Emily Bache`,
    title: 'TDD Training',
    abstract: `<div> 
        <p>
        Test-Driven Development is a cornerstone of agile development
        but often misunderstood. When done well, it leads to higher quality code,
        a comprehensive suite of unit tests and more frequent opportunities for
        integration. It’s not easy to learn though, and not the kind of thing you
        can hope to be able to do well after a short training course. The aim of
        this course is not to teach you TDD, but to help you to understand what
        TDD actually is. You should come away with enough knowledge about the
        technique that can decide whether it’s worth the effort to learn it
        properly, and what that would take.
        </p>
        <p>
        After the training, participants should be able to:
        <br/>- Explain why TDD is a great approach
        <br/>- Start a conversation with your team about why and how to adopt TDD
        <br/>- Find good practice activities you can do in order to improve your TDD skills
        </p>
        <p>Expectations on Participants</p>
        <p>This course is for software developers new to the software crafting
        community who are looking to understand what Test-Driven Development is
        and why it’s useful. The session includes hands-on exercises, so you
        will need to bring a laptop.
        </p>
        <ul>
          <li>session cap on 30 participants</li>
          <li>laptop required</li>
        </ul>
        </div>
        `,
    bio: `<div>
        <p>Emily is a Technical Agile Coach with ProAgile and works with developers to improve skills and promote technical excellence. She has worked with software development for over 20 years in diverse organizations from start-up to large enterprise. Emily has written two books about teaching and learning agile technical practices, and authored Pluralsight courses about unit testing. Emily is also a frequent speaker and keynote speaker at conferences including NDC, Jax, Craft, ACCU, Europython, Agile Testing Days and the Agile Alliance conference. Originally from the UK, she currently lives in Gothenburg, Sweden.</p>
        </div>`,
  },
  trunk_based_development: {
    author: `<div><a href="https://www.linkedin.com/in/julian-gode%C5%A1a-17a166238/" title="Follow Julian on LinkedIn!" target="_blank">Julian Godeša</a> &amp; <a href="https://www.linkedin.com/in/kateryna-oliferchuk-a52b7967/" title="Follow Kateryna on LinkedIn!" target="_blank">Kateryna Oliferchuk</a></div>`,
    title:
      'Trunk Based Development - unlock fast feedback and deliver high quality.',
    abstract: `<div>
        <p>Embracing the method of trunk based development will reduce time-to-market, improve team's collaboration, foster product ownership, catch problems early and deliver high quality. In our session, we will introduce the concept of trunk based development, discuss with you and other engineers the challenges of today's predominant open source branching model and present the first steps you can take to unlock the potential of trunk based development.
        </p>
        <ul>
          <li>session cap on 20 participants</li>
        </ul>
        </div>
        `,
    bio: `<div>
          <p>Kateryna (she/her) is a full stack and data engineer with almost 10 years of experience. She has over the years gained experiences in software architecture and DevOps patterns, tools and services. She is enthusiastic about Scala, loves good challenges and most importantly she is a great team player. Learning and sharing knowledge with others is very important to her.</p>
          <p>Hi, I am Julian 👋. A continuous learner with a strong passion for everything Agile. I have many years experience working as a Frontend, Backend and Full-Stack developer for various companies and now shifted my professional focus on enabling the people building great software. When I am not sitting in front of the computer, you can find me walking my dog or riding the bike through beautiful Berlin.</p>
        </div>`,
  },
  example_mapping: {
    author: `Seb Rose`,
    title: 'Example Mapping: Slice Any Story into Testable Examples',
    abstract: `<div>
        <p><a href="https://cucumber.io/blog/bdd/example-mapping-introduction/" title="Example Mapping introduction">Example mapping</a>
        is a simple but powerful technique for structuring the conversation you
        need to have before a user story goes into development. If you are
        struggling with user stories that are too big, or hard to test, or
        you're finding that the team are not all on the same page about the
        scope of a user story, Example Mapping could be just what you need.
        Using a regular pack of coloured index cards, we'll work in groups to
        practice breaking down the details of a user story, capturing the
        business rules, examples of those rules, and any questions or
        assumptions that emerge. Example mapping is a great input to a BDD or
        ATDD process, but that's not essential. You'll still get a lot out of
        this conversation technique even if you don't turn the examples into
        automated tests.
        </p>
        <ul>
          <li>no laptop required</li>
        </ul>
        </div>
        `,
    bio: `<div>
          <p>Consultant, coach, trainer, analyst, and developer for over 40 years.</p>
          <p>Seb has been a consultant, coach, designer, analyst and developer for over 40 years. He has been involved in the full development lifecycle with experience that ranges from architecture to support, from BASIC to Ruby.</p>
          <p>During his career, he has worked for companies large (e.g. IBM, Amazon) and small, and has extensive experience of failed projects. He's now Developer Advocate with SmartBear Advantage, promoting better ways of working to the software development community.</p>
          <p>Regular speaker at conferences and occasional contributor to software journals. Co-author of the BDD Books series "Discovery” and "Formulation" (Leanpub), lead author of “The Cucumber for Java Book” (Pragmatic Programmers), and contributing author to “97 Things Every Programmer Should Know” (O’Reilly).</p>
        </div>`,
  },
  golden_master: {
    author: `Michelle Avomo`,
    title: 'Legacy code: Add a reliable test harness (quickly) and refactor!',
    abstract: `<div>
          <p>A legacy code comes with lots of issues, the first one for any developer is the scarcity of tests that document the current state of the application and offer a reliable feedback loop.</p>
          <p>As a craftsperson, having to code without the security of a test harness is not a viable option but, knowing how to test a legacy code requires some specific techniques.</p>
          <p>In this session, we will explore the Golden Master technique and the characterization tests on a home-made kata (Kotlin & JavaScript).</p>
          <p/>
          <p>At the end of this session, the attendees should have learnt how to manually generate a golden master and the limits of that technique.</p>
          <p>The attendees might also learn about the characterization tests.</p>
          <p>The kata is a hands-on session, please bring a laptop and make sure you have a working IDE.</p>
          <p>Expectations on Participants</p>
          <p>This course is for software developers new to the software crafting community who are :
          <br/>- Working with legacy code and do not yet know how to add tests on a code without tests
          <br/>- Working with legacy code and do not yet know how to break dependencies and improve the design of the code
          <br/>- Looking for a (new) kata on legacy code and how to facilitate it for sharing the same topic back at their work
          <br/>- Willing to try a pair-programming session
          </p>
          <ul>
            <li>session cap on 20 participants</li>
            <li>laptop required</li>
          </ul>
        </div>`,
    bio: `<div>
        <p>After a couple of years learning and training the "Clean Code" book materials, Michelle became a daily practitioner of the Xtreme Programming Practices and a mentor for some.</p>
        <p>She discovered and mastered most of her knowledge within the software crafters meetups and conferences.</p>
        <p>She works in Paris as a Senior Developer consultant at CodeWorks and is always keen to learn and train on how to write maintainable code.</p>
        </div>`,
  },
  atdd_bdd: {
    author: `Markus Gärtner`,
    title: 'Automating your examples with ATDD/BDD',
    abstract: `<div>
        <p>Once you have identified examples for upcoming changes in your applications and described them in an expressive way, it would be helpful if you could use the examples to help implement the functionality along the way, providing you with the confidence that you might be potentially done with the implementation while developing your application on the one hand, and keeping it up to date with future changes by turning them into a living documentation.</p>
        <p>We'll take a closer look into how to automate examples from an example mapping session, watch out for traps and pitfalls that you might bring to the table, and give you a good starting point to bring meaningful automated tests that describe your application - maybe without shooting yourself into the foot along the way.</p>
        <ul>
          <li>session cap on 20 participants</li>
        </ul>
        </div>`,
    bio: `<div>
        <p>Markus Gärtner works as Organizational Design Consultant,
        Certified Scrum Trainer (CST) and Agile Coach for it-agile GmbH,
        Hamburg, Germany. Markus, author of ATDD by Example - A Practical Guide
        to Acceptance Test-Driven Development, a student of the work of
        Jerry Weinberg, received the Most Influential Agile Testing Professional
        Person Award in 2013, and contributes to the Softwerkskammer, the German
        Software Craftsmanship movement. Markus regularly presents at Agile and
        testing conferences all over the globe, as well as dedicating himself to
        writing about agile software development, software craftsmanship, and
        software testing, foremost in an Agile context. He maintains a personal
        blog at <a href="http://www.shino.de/blog">www.shino.de/blog</a>.</p>
        </div>`,
  },
  refactoring: {
    author: `Nicole Rauch`,
    title: 'Refactoring for Deeper Understanding',
    abstract: `<div>
        <p>In this session we will look at what I consider the biggest code smell in existence: Code that is difficult to understand. I will show you how you can make your code more readable and at the same time more robust and correct, and we will also touch upon the background of this refactoring. While we're at it, I'll point out some other benefits you can reap if you extend that technique, and will hint at what else is in stock for those who are willing to go a few extra miles.</p>
        <ul>
          <li>laptop required</li>
        </ul>
        </div>`,
    bio: `<div>
        <p>Nicole Rauch is an independent software developer and development coach with a solid background in compiler construction and formal methods. Her focus is on Domain-Driven Design, React/Redux with TypeScript as well as Clean Code and the restructuring of large Java legacy code applications. Nonetheless, her secret love is for functional programming. Furthermore, she is part of the organizers' committee of a number of conferences and co-founded Softwerkskammer, the german-speaking Software Craftsmanship community.</p>
        </div>`,
  },
  property_based_testing: {
    author: `<div>Romeu Moura &amp; Dorra Bartaguiz</div>`,
    title:
      'Different ways of using Property Based testing in your code next Monday',
    abstract: `<div>
          <p>Property based testing is one of the cheapest technique you can learn today and start using on your day to day job Monday and yield benefits in your life<br>It will help you test for things you have not completely seen<br>It will help you challenge your assumptions about the domain of your code<br>Let’s explore this together in an ensemble programming session with debates </p>
        </div>`,
    bio: `<div>
          <p>Dorra Bartaguiz is technical coach and developer at Arolla, co-author of "Software Craft" - Dunod. She like discussing about subjects like architecture, tests, xDD, and team organization. She is active in women tech communities.</p>
        </div>`,
  },
  hexagonal_architecture: {
    author: `Thomas Pierrain (use case driven)`,
    title: 'Hexagonal Architecture & Beyond',
    abstract: `<div>
        <p>Hexagonal architecture (a.k.a. Ports and Adapters) is a fabulous pattern that has more advantages than the ones for which it has been originally created.</p>
        <p>One can think in an orthodox vision that patterns do not evolve. That it is important to keep Alistair Cockburn&rsquo;s pattern like it was described back in the days.</p>
        <p>One can think that some patterns may evolve, that Hexagonal Architecture has more facets than we think. This session will present both the original pattern in detail, and some alternative versions (related to Domain Driven Design).</p></div>`,
    bio: `<div>
          <p>VP of Engineering in a booming European scale-up (Agicap), Thomas is a former entrepreneur, consultant, architect and eXtreme Programmer obsessed with use cases during more than 25 years (as opposed to the solution-oriented approach that we tend to choose in our job a little too much). Co-organizer of the DDD France (and former BDD Paris) meetups, Thomas likes to use autonomy, DDD and TDD to boost his efficiency and that of others at work.</p>
          <p>Long-time promoter of hexagonal architecture (through articles, talks, trainings or live coding sessions - including one with Alistair Cockburn) Thomas is now advocating for a specific style of TDD called Outside-in Diamond 🔷 TDD . This fits perfectly with Alistair's pattern, and also allows for more secure, antifragile, and Domain-Driven tests to be written.</p>
        </div>
        `,
  },
  ensemble_exploratory_testing: {
    author: `Lisi Hocke`,
    title: 'Ensemble Exploratory Testing',
    abstract: `<div>
        <p>Ever wondered how experienced testers provide feedback quickly, discover unknown unknowns, and find that issue yet again on the first touch of the application? Let's lift the curtain together and discover the magic behind exploratory testing - as an ensemble. Whether you identify yourself as a developer, product person, tester, or anything else, whether you consider yourself a newbie or rather experienced, you are welcome to join this ensemble. Let's practice and explore together!
        </p>
        <p>
        Main learnings:
        <br>- Learn the basics of the ensemble approach and practice them hands-on
        <br>- Experience the benefits of ensemble testing
        <br>- Practice exploratory testing hands-on together
        </p>
        <ul>
          <li>session cap on 25 participants</li>
          <li>laptop required</li>
        </ul>
        </div>
        `,
    bio: `<div>
          <p>Lisi found tech as her place to be in 2009 and grew as a specialized
          generalist ever since. She's passionate about the whole-team approach
          to holistic testing and quality and enjoys experimenting and learning
          continuously. Building great products which deliver value together
          with great people motivates her and lets her thrive. Having received a
          lot from communities, she's paying it forward by sharing her stories
          and learning in public. She blogs at <a href="https://www.lisihocke.com">www.lisihocke.com</a>.
          In her free time, she plays indoor volleyball or delves into computer
          games and stories of all kinds.</p>
        </div>`,
  },
  design_for_longevity: {
    author: `Samir Talwar`,
    title: 'Designing for Longevity',
    abstract: `<div>
        <p>When we make software, we don’t often think about its life span. We write code to fulfil today’s goal, not to iterate upon it tomorrow.<br>And yet, we know that piling feature upon feature makes for an unstable tower, and that programmers new to a project can take weeks or even months to become productive.<br>Join us as we explore what it means to design a codebase you can live in, not just visit. In this workshop, we will explore:<br>- the properties of long-lasting code,<br>- when aiming for longevity is important, and when it is not, and<br>- what kind of trade-offs we need to make.</p>
        <ul>
          <li>session cap on 20 participants</li>
        </ul>
        </div>
        `,
    bio: ``,
  },
} as const;

export type AbstractKey = keyof typeof abstracts;
