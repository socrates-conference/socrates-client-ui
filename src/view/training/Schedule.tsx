import React, { useEffect, useRef } from 'react';
import './Schedule.scss';
import { useLocation } from 'react-router-dom';

type SessionType = 'beginner' | 'experienced';

type Track = { name: string; topic: string; type: SessionType };
interface Session {
  start: string;
  end: string;
  track1: Track;
  track2: Track;
  track3: Track;
}

const sessions: Session[] = [
  // {
  //   start: '9:00',
  //   end: '10:30',
  //   track1: {
  //     name: 'Romeu Moura',
  //     topic: 'TDD: Beyond the Intro',
  //     type: 'experienced',
  //   },
  //   track2: {
  //     name: 'Javiera Laso',
  //     topic: 'Enforcing Architecture using Tests',
  //     type: 'experienced',
  //   },
  //   track3: {
  //     name: 'Diana Montalion & Tobias G',
  //     topic: 'Intro to DDD, part 1',
  //     type: 'beginner',
  //   },
  // },
  // {
  //   start: '11:00',
  //   end: '12:30',
  //   track1: {
  //     name: 'Alexandra Schladebeck',
  //     topic: 'Exploratory Testing',
  //     type: 'beginner',
  //   },
  //   track2: {
  //     name: 'Raimo Radczewski & Chris Neuroth',
  //     topic: 'Modern Continuous Delivery Pipelines',
  //     type: 'experienced',
  //   },
  //   track3: {
  //     name: 'Diana Montalion & Tobias G',
  //     topic: 'Intro to DDD, part 2',
  //     type: 'beginner',
  //   },
  // },
  // {
  //   start: '14:00',
  //   end: '15:30',
  //   track1: {
  //     name: 'Lisi Hocke',
  //     topic: 'Ensemble Exploratory Testing',
  //     type: 'experienced',
  //   },
  //   track2: {
  //     name: 'Sandra Parsick',
  //     topic: 'Testcontainers, Automation, Infrastructure',
  //     type: 'experienced',
  //   },
  //   track3: {
  //     name: 'Vasco Duarte',
  //     topic:
  //       '"#NoEstimates Applied to Software Development\nHow to deliver faster, and with more certainty without estimates"',
  //     type: 'beginner',
  //   },
  // },
  // {
  //   start: '16:00',
  //   end: '17:30',
  //   track1: {
  //     name: 'Markus Gärtner',
  //     topic: 'Software Craft Pathfinder',
  //     type: 'beginner',
  //   },
  //   track2: {
  //     name: 'Aki Salmi',
  //     topic: 'Take a Mess, Fix the Mess',
  //     type: 'experienced',
  //   },
  //   track3: {
  //     name: 'Woody Zuill',
  //     topic: 'Software Teaming',
  //     type: 'beginner',
  //   },
  // },
];

const scheduleAnchor = '#schedule';
export default function Schedule() {
  const scheduleElement = useRef<HTMLAnchorElement>(null);

  const { hash } = useLocation();

  useEffect(() => {
    if (scheduleElement.current && hash === scheduleAnchor) {
      scheduleElement.current.scrollIntoView({ behavior: 'smooth' });
    }
  }, [hash]);

  return (
    <div className="schedule">
      <div className="heading">
        <a href={scheduleAnchor} ref={scheduleElement}>
          <h3 className="heading-text">
            <span className="heading-anchor">#</span> Training Day Schedule
          </h3>
        </a>
      </div>

      {/*<div className="session-card">*/}
      {/*  <div className="time">*/}
      {/*    <span>8:45 - 9:00</span>*/}
      {/*  </div>*/}
      {/*  <div className="welcome-block">*/}
      {/*    <div className="welcome-location"></div>*/}
      {/*    <div className="name">Kickoff</div>*/}
      {/*  </div>*/}
      {/*</div>*/}

      {/*<div className="legend">*/}
      {/*  <p className="beginner">Beginner Session</p>*/}
      {/*  <p className="experienced">Experienced Session</p>*/}
      {/*</div>*/}

      {sessions.map((session, index) => (
        <div className="session-card" key={index}>
          <div className="time">
            <span>
              {session.start} - {session.end}
            </span>
          </div>
          <div className={`track ${session.track1.type}`}>
            <div className="track-number">Track 1 - Hamburg</div>
            <div className="name">{session.track1.name}</div>
            <div className="topic">{session.track1.topic}</div>
          </div>
          <div className={`track ${session.track2.type}`}>
            <div className="track-number">Track 2 - Erfurt</div>
            <div className="name">{session.track2.name}</div>
            <div className="topic">{session.track2.topic}</div>
          </div>
          <div className={`track ${session.track3.type}`}>
            <div className="track-number">Track 3 - Hannover</div>
            <div className="name">{session.track3.name}</div>
            <div className="topic">{session.track3.topic}</div>
          </div>
        </div>
      ))}
    </div>
  );
}
