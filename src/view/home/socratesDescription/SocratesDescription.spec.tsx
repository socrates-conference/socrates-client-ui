import React from 'react';
import { shallow } from 'enzyme';
import SocratesDescription from './SocratesDescription';
describe('(Component) SoCratesDescription', () => {
  const wrapper = shallow(<SocratesDescription />);
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
