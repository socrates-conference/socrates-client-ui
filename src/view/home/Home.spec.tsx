import React from 'react';
import { shallow } from 'enzyme';
import { Home } from './Home';
describe('(Component) Home', () => {
  const wrapper = shallow(<Home />);
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
