import {
  NewsletterSignUpContainer,
  NewsletterSignUpContainerProps,
} from './NewsletterSignUpContainer';
import { mount, ReactWrapper, shallow, ShallowWrapper } from 'enzyme';
import React, { ReactElement } from 'react';
import { REMOVE_SUCCEEDED } from '../../newsletter-signout/NewsletterSignOutConstants';
const defaultProps = {
  message: '',
  signUp: () => {},
};

const _mount = (props: NewsletterSignUpContainerProps) => {
  const finalProps = { ...defaultProps, ...props };
  return mount(<NewsletterSignUpContainer {...finalProps} />);
};

function getNestedInput(
  newsletter: ReactWrapper<ReactElement> | ShallowWrapper<ReactElement>,
  selector: string = '#newsletter-name',
): HTMLInputElement {
  return newsletter.find(selector).instance() as unknown as HTMLInputElement;
}

describe('NewsletterSignUpContainer', () => {
  let mockSignUp: (username: string, email: string) => any,
    newsletter: ReactWrapper<ReactElement> | ShallowWrapper<ReactElement>;
  beforeEach(() => {
    mockSignUp = jest.fn();
    newsletter = _mount({
      signUp: mockSignUp,
    });
  });
  afterEach(() => {
    newsletter.unmount();
  });

  function setInputValue(selector: string, value: string) {
    const wrapper = newsletter.find(selector);
    const node: HTMLInputElement =
      wrapper.instance() as unknown as HTMLInputElement;
    node.value = value;
    wrapper.simulate('change', {
      target: {
        value,
      },
    });
    newsletter.update();
  }

  function fillForm(name: string, email: string) {
    setInputValue('#newsletter-name', name);
    setInputValue('#newsletter-email', email);
  }

  function fillFormActivateCheckboxAndClickButton(name: string, email: string) {
    fillForm(name, email);
    const wrapper = newsletter.find('#newsletter-dataPrivacy');
    const node: HTMLInputElement = getNestedInput(
      newsletter,
      '#newsletter-dataPrivacy',
    );
    node.checked = true;
    wrapper.simulate('change', {
      target: {
        value: 'dataPrivacyConfirmed',
      },
    });
    newsletter.update();
    newsletter.find('#newsletter-form-button').simulate('click');
  }

  it('empty name is invalid', () => {
    expect(getNestedInput(newsletter, '#newsletter-name').className).toContain(
      'is-invalid',
    );
  });
  it('filled name is valid', () => {
    fillForm('name', '');
    expect(getNestedInput(newsletter, '#newsletter-name').className).toContain(
      'is-valid',
    );
  });
  it('empty email is invalid', () => {
    expect(getNestedInput(newsletter, '#newsletter-email').className).toContain(
      'is-invalid',
    );
  });
  it('filled well formatted email is valid', () => {
    fillForm('name', 'valid@example.com');
    expect(getNestedInput(newsletter, '#newsletter-email').className).toContain(
      'is-valid',
    );
  });
  it('filled with invalid formatted email is invalid', () => {
    fillForm('name', 'invalidExample.com');
    expect(getNestedInput(newsletter, '#newsletter-email').className).toContain(
      'is-invalid',
    );
  });
  it('should dispatch SignUp command when form is submitted', () => {
    fillFormActivateCheckboxAndClickButton('name', 'valid@example.com');
    expect(mockSignUp).toHaveBeenCalledTimes(1);
  });
  describe('when receiving props', () => {
    it('should render with message', () => {
      newsletter = shallow(
        <NewsletterSignUpContainer
          message={REMOVE_SUCCEEDED}
          signUp={() => {}}
        />,
      );
      expect(newsletter).toMatchSnapshot();
    });
  });
});
