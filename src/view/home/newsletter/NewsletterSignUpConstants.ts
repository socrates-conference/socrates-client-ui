export const ADD_SUCCEEDED: string =
  'Thank you for caring about Software Craft. You will now receive the SoCraTes newsletter.';
export const ADD_FAILED: string =
  'We currently cannot add you to the list. Please try again later.';
export const EMPTY: string = '';
