import { Description, Name, URL } from '../../common/types';

export type PartnerConference = {
  name: Name;
  description: Description;
  url: URL;
};
