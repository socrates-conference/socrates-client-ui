import React from 'react';
import { shallow } from 'enzyme';
import PartnerConferences from './PartnerConferences';
describe('(Component) PartnerConferences', () => {
  const socratesChile = {
    name: 'SoCraTes Chile',
    description: '1 July 2016, Santiago de Chile',
    url: 'https://www.socrates-conference.cl/',
  };
  const socratesCanaries = {
    name: 'SoCraTes Canaries',
    description: '6 - 9 April 2017, Gran Canaria, Spain',
    url: 'https://www.socracan.com',
  };
  const conferences = [socratesChile, socratesCanaries];
  const wrapper = shallow(<PartnerConferences conferences={conferences} />);
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders all conferences', () => {
    expect(wrapper.find('PartnerConferenceComponent')).toHaveLength(2);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
