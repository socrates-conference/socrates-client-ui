import axios from 'axios';
import api from './api';
jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

describe('api', () => {
  it('add interested person resolves to true when server returns true', (done) => {
    const promise = Promise.resolve(true);
    mockedAxios.post.mockImplementation(() => {
      return promise;
    });
    api.addInterestedPerson('name', 'email@example.com').then((result) => {
      (axios.post as any).mockRestore();
      expect(result).toBeTruthy();
      done();
    });
  });
  it('add interested person resolves to false when server returns false', (done) => {
    const promise = Promise.resolve(false);
    mockedAxios.post.mockImplementation(() => {
      return promise;
    });
    api.addInterestedPerson('name', 'email@example.com').then((result) => {
      mockedAxios.post.mockRestore();
      expect(result).toBeFalsy();
      done();
    });
  });
  it('add interested person rejects when server rejects', (done) => {
    const promise = Promise.reject(new Error('error'));
    mockedAxios.post.mockImplementation(() => {
      return promise;
    });

    api.addInterestedPerson('name', 'email@example.com').catch((result) => {
      mockedAxios.post.mockRestore();
      expect(result.message).toEqual('error');
      done();
    });
  });
});
