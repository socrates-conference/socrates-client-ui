export const CONFIRM_SUCCEEDED: string =
  'You have successfully signed up for the SoCraTes Germany newsletter.';
export const CONFIRM_FAILED: string =
  'We currently cannot confirm your newsletter subscription. Please try again later.';
export const EMPTY: string = '';
