import NewsletterEvents from '../events/newsletterEvents';
import type { NewsletterEvent } from '../events/newsletterEvents';
import { EMPTY } from './NewsletterConfirmConstants';
type State = {
  message: string;
};
const INITIAL_STATE = {
  message: EMPTY,
};

const newsletterConfirmReducer = (
  state: State = INITIAL_STATE,
  event: NewsletterEvent,
) => {
  switch (event.type) {
    case NewsletterEvents.CONFIRM_SUCCEEDED:
    case NewsletterEvents.CONFIRM_FAILED:
      return {
        message: event.message || EMPTY,
      };

    default:
      return state;
  }
};

export default newsletterConfirmReducer;
