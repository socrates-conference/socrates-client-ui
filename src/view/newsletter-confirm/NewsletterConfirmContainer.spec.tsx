import { shallow } from 'enzyme';
import React from 'react';
import { NewsletterConfirmContainer } from './NewsletterConfirmContainer';
import { CONFIRM_SUCCEEDED } from './NewsletterConfirmConstants';
import { History, Location, LocationState } from 'history';
describe('NewsletterConfirmContainer', () => {
  let newsletterConfirm;
  let root: HTMLElement;
  it('should render without exploding', () => {
    newsletterConfirm = shallow(
      <NewsletterConfirmContainer
        message=""
        history={{} as History}
        location={{
          pathname: '',
          search: '',
          hash: '',
          state: {} as LocationState as Location,
        }}
        match={{ params: { data: '' }, isExact: true, path: '', url: '' }}
      />,
      {
        attachTo: root,
      },
    );
    expect(newsletterConfirm).toHaveLength(1);
  });
  it('should render correctly', () => {
    newsletterConfirm = shallow(
      <NewsletterConfirmContainer
        message=""
        history={{} as History}
        location={{
          pathname: '',
          search: '',
          hash: '',
          state: {} as LocationState as Location,
        }}
        match={{ params: { data: '' }, isExact: true, path: '', url: '' }}
      />,
      {
        attachTo: root,
      },
    );
    expect(newsletterConfirm).toMatchSnapshot();
  });
  describe('when receiving props', () => {
    it('should render with params', () => {
      const match = {
        params: {
          data: 'test@example.com',
        },
        isExact: true,
        path: '',
        url: '',
      };
      newsletterConfirm = shallow(
        <NewsletterConfirmContainer
          message=""
          history={{} as History}
          location={{
            pathname: '',
            search: '',
            hash: '',
            state: {} as LocationState as Location,
          }}
          match={match}
        />,
        {
          attachTo: root,
        },
      );
      expect(newsletterConfirm).toMatchSnapshot();
    });
    it('should render with message', () => {
      newsletterConfirm = shallow(
        <NewsletterConfirmContainer
          message={CONFIRM_SUCCEEDED}
          history={{} as History}
          location={{
            pathname: '',
            search: '',
            hash: '',
            state: {} as LocationState as Location,
          }}
          match={{ params: { data: '' }, isExact: true, path: '', url: '' }}
        />,
        {
          attachTo: root,
        },
      );
      expect(newsletterConfirm).toMatchSnapshot();
    });
  });
});
