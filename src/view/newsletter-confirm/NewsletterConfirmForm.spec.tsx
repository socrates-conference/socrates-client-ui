import React from 'react';
import NewsletterConfirmForm from './NewsletterConfirmForm';
import { shallow } from 'enzyme';

describe('(Component) NewsletterConfirmForm', () => {
  const wrapper = shallow(
    <NewsletterConfirmForm
      email=""
      hasValidEmail={false}
      onEmailChange={() => {}}
      message=""
      onNameChange={() => {}}
      onSubmit={() => {}}
      isDisabled={true}
      consentKey=""
      onConsentKeyChange={() => {}}
      isNewsletterConfirmedChecked={true}
      newsletterConfirmedChange={() => {}}
      isDataPrivacyConfirmedChecked={true}
      dataPrivacyConfirmedChange={() => {}}
    />,
  );
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
