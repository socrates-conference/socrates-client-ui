import React from 'react';

export type Props = {
  parentId: string;
  index: number;
  title: string;
  content: any;
};
export default function AccordionItem(props: Props) {
  const id = `${props.parentId}-item-${props.index}`;
  return (
    <div className="card border-secondary">
      <div id={id + '-header'} className="card-header">
        <h4 className="mb-0">
          <button
            className="btn btn-link"
            data-toggle="collapse"
            data-target={'#' + id}
            aria-expanded="false"
            aria-controls={id}
          >
            {props.title}
          </button>
        </h4>
      </div>
      <div
        id={id}
        className="collapse"
        aria-labelledby={'header' + id}
        data-parent={'#' + props.parentId}
      >
        <div className="card-body">{props.content}</div>
      </div>
    </div>
  );
}
