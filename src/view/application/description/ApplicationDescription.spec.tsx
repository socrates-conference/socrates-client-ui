import React from 'react';
import ApplicationDescription from './ApplicationDescription';
import { shallow } from 'enzyme';
describe('(Component) ApplicationDescription', () => {
  it('renders without exploding', () => {
    const wrapper = shallow(<ApplicationDescription />);
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    const wrapper = shallow(<ApplicationDescription />);
    expect(wrapper).toMatchSnapshot();
  });
});
