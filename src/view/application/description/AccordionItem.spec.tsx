import React from 'react';
import { shallow } from 'enzyme';
import AccordionItem from './AccordionItem';
describe('(Component) ApplicationDescriptionItem', () => {
  it('renders without exploding', () => {
    const wrapper = shallow(
      <AccordionItem
        title="Title"
        content="Content"
        parentId="parent"
        index={0}
      />,
    );
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    const wrapper = shallow(
      <AccordionItem
        title="Title"
        content="Content"
        parentId="parent"
        index={0}
      />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
