import React from 'react';
import { shallow } from 'enzyme';
import Accordion from './Accordion';
describe('(Component) Accordion', () => {
  const entries = [
    {
      title: 'Title',
      content: 'Content',
    },
  ];
  it('renders without exploding', () => {
    const wrapper = shallow(
      <Accordion parentId="test" entries={entries} startIndex={2} />,
    );
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    const wrapper = shallow(
      <Accordion parentId="test" entries={entries} startIndex={2} />,
    );
    expect(wrapper).toMatchSnapshot();
  });
});
