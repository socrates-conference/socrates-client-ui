import React from 'react';
import AccordionItem from './AccordionItem';

type AccordionEntry = {
  title: string;
  content: any;
};
type Props = {
  parentId: string;
  entries: Array<AccordionEntry>;
  startIndex?: number;
};
export default function Accordion(props: Props) {
  const startIndex = props.startIndex !== undefined ? props.startIndex : 0;
  return (
    <div>
      {props.entries.map((item, index) => (
        <AccordionItem
          parentId={props.parentId}
          title={item.title}
          content={item.content}
          index={startIndex + index}
          key={`${props.parentId}-item-${startIndex + index}`}
        />
      ))}
    </div>
  );
}
