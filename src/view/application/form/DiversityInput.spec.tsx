import React from 'react';
import { DiversityInput } from './DiversityInput';
import { render, mount } from 'enzyme';
import { spy } from 'sinon';
describe('(Component) DiversityInput', () => {
  it('renders without exploding', () => {
    const wrapper = _render();

    expect(wrapper).toHaveLength(1);
  });
  it('renders three radio buttons', () => {
    const wrapper = _mount();

    expect(
      wrapper.find({
        type: 'radio',
      }),
    ).toHaveLength(3);
  });
  it('shows a text input when other is selected', () => {
    const wrapper = _mount();

    wrapper.find('#other').simulate('change', {
      target: {
        value: 'other',
      },
    });
    expect(wrapper.find('#detailsForOther')).toHaveLength(1);
  });
  it('does not show the text input when yes is selected', () => {
    const wrapper = _mount();

    wrapper.find('#yes').simulate('change', {
      target: {
        value: 'yes',
      },
    });
    expect(wrapper.find('#detailsForOther')).toHaveLength(0);
  });
  it('does not show the text input when no is selected', () => {
    const wrapper = _mount();

    wrapper.find('#no').simulate('change', {
      target: {
        value: 'no',
      },
    });
    expect(wrapper.find('#detailsForOther')).toHaveLength(0);
  });
  it('hides the text input when other and then yes is selected', () => {
    const wrapper = _mount();

    wrapper.find('#other').simulate('change', {
      target: {
        value: 'other',
      },
    });
    wrapper.find('#yes').simulate('change', {
      target: {
        value: 'yes',
      },
    });
    expect(wrapper.find('#detailsForOther')).toHaveLength(0);
  });
  it('when yes is selected it calls callback with yes', () => {
    const callback = spy();

    const wrapper = _mount({
      onDiversityChange: callback,
    });

    wrapper.find('#yes').simulate('change', {
      target: {
        value: 'yes',
      },
    });
    expect(callback.withArgs('yes').calledOnce).toBe(true);
  });
  it('when no is selected it calls callback with yes', () => {
    const callback = spy();

    const wrapper = _mount({
      onDiversityChange: callback,
    });

    wrapper.find('#no').simulate('change', {
      target: {
        value: 'no',
      },
    });
    expect(callback.withArgs('no').calledOnce).toBe(true);
  });
  it('when other is selected it calls callback with text input value', () => {
    const callback = spy();

    const wrapper = _mount({
      onDiversityChange: callback,
    });

    wrapper.find('#other').simulate('change', {
      target: {
        value: 'other',
      },
    });
    wrapper.find('#detailsForOther').simulate('change', {
      target: {
        value: 'some text',
      },
    });
    expect(callback.withArgs('some text').calledOnce).toBe(true);
  });
});
const defaultProps = {
  diversitySelected: '',
  onDiversityChange: () => {},
};

const _render = (props?: any) => {
  const finalProps = { ...defaultProps, ...props };
  return render(<DiversityInput {...finalProps} />);
};

const _mount = (props?: any) => {
  const finalProps = { ...defaultProps, ...props };
  return mount(<DiversityInput {...finalProps} />);
};
