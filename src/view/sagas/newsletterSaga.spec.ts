import axios from 'axios';
import rootReducer, { NewsletterState } from '../reducers/rootReducer';
import SagaTester from 'redux-saga-tester';
import rootSaga from './rootSaga';
import {
  ADD_FAILED,
  ADD_SUCCEEDED,
  EMPTY,
} from '../home/newsletter/NewsletterSignUpConstants';
import { signOut, signUp } from '../commands/newsletterCommand';
import NewsletterEvents from '../events/newsletterEvents';
import {
  REMOVE_FAILED,
  REMOVE_SUCCEEDED,
} from '../newsletter-signout/NewsletterSignOutConstants';
import config from '../../config';

const backend = config.apiEndpoints.subscriptions;

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

describe('NewsletterSaga', () => {
  let tester: SagaTester<{ newsletter: NewsletterState }>;
  beforeEach(() => {
    tester = new SagaTester({
      initialState: {
        newsletter: {
          message: EMPTY,
        },
      },
      // @ts-ignore
      reducers: rootReducer,
    });
    tester.start(rootSaga);
  });
  afterEach(() => {
    tester.reset();
  });
  describe('when signUp is successful', () => {
    beforeEach(async () => {
      mockedAxios.post.mockImplementation(() => Promise.resolve(true));
      tester.dispatch(signUp('test', 'test@test.de'));
      await tester.waitFor(NewsletterEvents.ADD_SUCCEEDED);
    });
    it('should set ADD_SUCCEEDED message', () => {
      expect(tester.getState().newsletter.message).toEqual(ADD_SUCCEEDED);
    });
    it('should call /interested-people api', () => {
      const [url, params] = mockedAxios.post.mock.calls[0];
      expect(url).toEqual(backend + '/subscribers');
      expect(params).toEqual({
        name: 'test',
        email: 'test@test.de',
      });
    });
    afterEach(() => mockedAxios.post.mockReset());
  });
  describe('when signUp is not successful', () => {
    beforeEach(async () => {
      mockedAxios.post.mockImplementation(() => Promise.resolve(false));
      tester.dispatch(signUp('test', 'test@test.de'));
      await tester.waitFor(NewsletterEvents.ADD_FAILED);
    });
    it('should set ADD_FAILED message', () => {
      expect(tester.getState().newsletter.message).toEqual(ADD_FAILED);
    });
    it('should call /interested-people api', () => {
      const [url, params] = mockedAxios.post.mock.calls[0];
      expect(url).toEqual(backend + '/subscribers');
      expect(params).toEqual({
        name: 'test',
        email: 'test@test.de',
      });
    });
    afterEach(() => mockedAxios.post.mockReset());
  });
  describe('when signUp results in an error', () => {
    beforeEach(async () => {
      mockedAxios.post.mockImplementation(() => Promise.reject());
      tester.dispatch(signUp('test', 'test@test.de'));
      await tester.waitFor(NewsletterEvents.ADD_FAILED);
    });
    it('should set ADD_FAILED message', () => {
      expect(tester.getState().newsletter.message).toEqual(ADD_FAILED);
    });
    it('should call /interested-people api', () => {
      const [url, params] = mockedAxios.post.mock.calls[0];
      expect(url).toEqual(backend + '/subscribers');
      expect(params).toEqual({
        name: 'test',
        email: 'test@test.de',
      });
    });
    afterEach(() => mockedAxios.post.mockReset());
  });
  describe('when signOut is successful', () => {
    beforeEach(async () => {
      mockedAxios.delete.mockImplementation(() => Promise.resolve(true));
      tester.dispatch(signOut('test@test.de'));
      await tester.waitFor(NewsletterEvents.REMOVE_SUCCEEDED);
    });
    it('should set REMOVE_SUCCEEDED message', () => {
      expect(tester.getState().newsletter.message).toEqual(REMOVE_SUCCEEDED);
    });
    it('should call /interested-people api', () => {
      const [url] = mockedAxios.delete.mock.calls[0];
      expect(url).toEqual(backend + '/subscribers/test@test.de');
    });
    afterEach(() => mockedAxios.delete.mockReset());
  });
  describe('when signOut is not successful', () => {
    beforeEach(async () => {
      mockedAxios.delete.mockImplementation(() => Promise.resolve(false));
      tester.dispatch(signOut('test@test.de'));
      await tester.waitFor(NewsletterEvents.REMOVE_FAILED);
    });
    it('should set REMOVE_FAILED message', () => {
      expect(tester.getState().newsletter.message).toEqual(REMOVE_FAILED);
    });
    it('should call /interested-people api', () => {
      const [url] = mockedAxios.delete.mock.calls[0];
      expect(url).toEqual(backend + '/subscribers/test@test.de');
    });
    afterEach(() => mockedAxios.delete.mockReset());
  });
  describe('when signOut results in an error', () => {
    beforeEach(async () => {
      mockedAxios.delete.mockImplementation(() => Promise.reject());
      tester.dispatch(signOut('test@test.de'));
      await tester.waitFor(NewsletterEvents.REMOVE_FAILED);
    });
    it('should set REMOVE_FAILED message', () => {
      expect(tester.getState().newsletter.message).toEqual(REMOVE_FAILED);
    });
    it('should call /interested-people api', () => {
      const [url] = mockedAxios.delete.mock.calls[0];
      expect(url).toEqual(backend + '/subscribers/test@test.de');
    });
    afterEach(() => mockedAxios.delete.mockReset());
  });
});
