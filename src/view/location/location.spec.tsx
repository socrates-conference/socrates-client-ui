import React from 'react';
import { shallow } from 'enzyme';
import Location from './Location';
describe('(Component) Format', () => {
  const wrapper = shallow(<Location />);
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
