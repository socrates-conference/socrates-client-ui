import React from 'react';
import { shallow } from 'enzyme';
import Accessibility from './Accessibility';
describe('(Component) Accessibility', () => {
  const wrapper = shallow(<Accessibility />);
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
