import React from 'react';
import NewsletterSignOutForm from './NewsletterSignOutForm';
import { shallow } from 'enzyme';
describe('(Component) NewsletterSignOutForm', () => {
  const wrapper = shallow(
    <NewsletterSignOutForm
      email=""
      hasValidEmail={false}
      message=""
      onEmailChange={() => {}}
      onSubmit={() => {}}
      isDisabled={true}
    />,
  );
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
