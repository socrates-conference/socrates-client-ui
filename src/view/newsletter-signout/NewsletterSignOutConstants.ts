export const REMOVE_SUCCEEDED: string =
  "You have successfully signed out of the SoCraTes Germany newsletter.\n\nWe're sorry you're leaving. Best of luck!";
export const REMOVE_FAILED: string =
  "We're sorry, unfortunately, we currently cannot remove you from the list.\n\nPlease try again later, or send us an email to info@socrates-conference.de";
export const EMPTY: string = '';
