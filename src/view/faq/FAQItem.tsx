import React from 'react';
import './faq.scss';
export type Props = {
  title: string;
  index: number;
  content: React.ReactNode;
  id?: string;
  open?: boolean;
};
export default function FAQItem({ index, content, title, id, open }: Props) {
  const faqId = id || 'faq-item' + index;
  return (
    <details id={faqId} open={open}>
      <summary className="summary-class">
        {index + 1}. {title}
      </summary>
      <div className="content-class">{content}</div>
    </details>
  );
}
