import React from 'react';
import { shallow } from 'enzyme';
import FAQItem from './FAQItem';
describe('(Component) Format', () => {
  const wrapper = shallow(
    <FAQItem index={1} title="something" content="An explanation." />,
  );
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
