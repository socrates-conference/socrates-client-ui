export const fatalError = (message: string) => ({
  type: 'FATAL_ERROR',
  message,
});
