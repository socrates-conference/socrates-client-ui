import React from 'react';
import { shallow } from 'enzyme';
import History from './History';
describe('(Component) History', () => {
  const wrapper = shallow(<History />);
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
