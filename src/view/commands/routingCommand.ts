export default class RoutingCommand {
  static get ROUTE_TO() {
    return 'RoutingCommand/ROUTE_TO';
  }
}
export const routeTo = (url: string) => ({
  type: RoutingCommand.ROUTE_TO,
  url,
});
