import React from 'react';
import { shallow } from 'enzyme';
import Format from './Format';
describe('(Component) Format', () => {
  const wrapper = shallow(<Format />);
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
