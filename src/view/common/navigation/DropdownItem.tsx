import { Link } from 'react-router-dom';
import React from 'react';
type DropdownItemProps = {
  visible: boolean;
  url: string;
  title: string;
  iconClass: string;
  onClick: () => void;
};
export default function DropdownItem(props: DropdownItemProps) {
  const linkClass = 'nav-link' + (props.visible ? ' d-block' : ' d-none');
  return (
    <Link
      className={linkClass}
      to={props.url}
      title={props.title}
      data-toggle="collapse"
      data-target=".navbar-collapse.show"
      onClick={props.onClick}
    >
      <span className={props.iconClass} />
      <span> {props.title}</span>
    </Link>
  );
}
