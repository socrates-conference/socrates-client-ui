import React from 'react';
import { shallow } from 'enzyme';
import Navigation from './Navigation';
describe('(Component) Navigation', () => {
  const wrapper = shallow(<Navigation />);
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
