import React from 'react';
import { shallow } from 'enzyme';
import NavigationLink from './NavigationLink';
import { faClock } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
describe('(Component) Navigation link', () => {
  const wrapper = shallow(
    <NavigationLink
      url="/url"
      title="Title"
      icon={<FontAwesomeIcon icon={faClock} />}
    />,
  );
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
