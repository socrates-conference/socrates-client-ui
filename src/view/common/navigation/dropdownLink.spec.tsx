import React from 'react';
import { shallow } from 'enzyme';
import DropdownLink from './DropdownLink';
describe('(Component) Dropdown item', () => {
  const wrapper = shallow(
    <DropdownLink
      visible={true}
      url="/url"
      title="Title"
      iconClass="iconClass"
    />,
  );
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
