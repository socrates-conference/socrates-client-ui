import React from 'react';
import { shallow } from 'enzyme';
import DropdownItem from './DropdownItem';
describe('(Component) Dropdown item', () => {
  const wrapper = shallow(
    <DropdownItem
      visible={true}
      url="/url"
      title="Title"
      iconClass="iconClass"
      onClick={() => {}}
    />,
  );
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
