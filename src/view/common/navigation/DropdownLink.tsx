import React from 'react';
type DropdownItemProps = {
  visible: boolean;
  url: string;
  title: string;
  iconClass: string;
};
export default function DropdownItem(props: DropdownItemProps) {
  const linkClass = 'nav-link' + (props.visible ? ' d-block' : ' d-none');
  return (
    <a
      className={linkClass}
      href={props.url}
      title={props.title}
      data-toggle="collapse"
      data-target=".navbar-collapse.show"
      onClick={() => {
        window.location.href = props.url;
      }} // TODO
    >
      <span className={props.iconClass} />
      <span> {props.title}</span>
    </a>
  );
}
