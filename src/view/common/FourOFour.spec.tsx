import React from 'react';
import { shallow } from 'enzyme';
import FourOFour from './FourOFour';

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useLocation: () => ({
    pathname: 'localhost:3000/404',
  }),
}));

describe('(Component) FourOFour', () => {
  const wrapper = shallow(<FourOFour />);
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
