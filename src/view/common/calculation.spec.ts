import { calculateTicket, calculateTicketPrice } from './calculation';

const SINGLE = {
  type: 'single',
  description: 'Single Room',
  pricePerNight: '64€',
  beds: 1,
  count: 1,
};

const TRAINING_DAY = {
  type: 'foundations',
  description: 'Training Day',
  price: '100€',
};
const SOCRATES = {
  type: 'socrates',
  description: 'SoCraTes',
  price: '176,30€',
};

const JAN_1: string = '2022-01-01';
const JAN_2: string = '2022-01-02';
const JAN_3: string = '2022-01-03';

describe('calculateTicketPrice:', () => {
  it('should return 0€ for an empty order', () => {
    expect(
      calculateTicketPrice({
        flatFeeItems: [],
        roomType: SINGLE,
        arrival: JAN_1,
        departure: JAN_1,
      }),
    ).toEqual('0,00€');
  });
  it('should return 64€ for one night in a single room', () => {
    expect(
      calculateTicketPrice({
        flatFeeItems: [],
        roomType: SINGLE,
        arrival: JAN_1,
        departure: JAN_2,
      }),
    ).toEqual('64,00€');
  });
  it('should return 128€ for two nights in a single room', () => {
    expect(
      calculateTicketPrice({
        flatFeeItems: [],
        roomType: SINGLE,
        arrival: JAN_1,
        departure: JAN_3,
      }),
    ).toEqual('128,00€');
  });
  it('should return 100€ for Training Day only', () => {
    expect(
      calculateTicketPrice({
        flatFeeItems: [TRAINING_DAY],
        roomType: SINGLE,
        arrival: JAN_1,
        departure: JAN_1,
      }),
    ).toEqual('100,00€');
  });
  it('should return 276,30€ for Training Day and SoCraTes', () => {
    expect(
      calculateTicketPrice({
        flatFeeItems: [TRAINING_DAY, SOCRATES],
        roomType: SINGLE,
        arrival: JAN_1,
        departure: JAN_1,
      }),
    ).toEqual('276,30€');
  });
});

describe('calculateTicket:', () => {
  it('should return 64€ for one night in a single room', () => {
    expect(
      calculateTicket({
        flatFeeItems: [],
        roomType: SINGLE,
        arrival: JAN_1,
        departure: JAN_2,
      }),
    ).toEqual({
      packages: [],
      accommodation: { label: 'Single Room (1 night)', price: '64,00€' },
      total: '64,00€',
      totalWithSponsoring: '64,00€',
    });
  });
  it('should return 128€ for two nights in a single room', () => {
    expect(
      calculateTicket({
        flatFeeItems: [],
        roomType: SINGLE,
        arrival: JAN_1,
        departure: JAN_3,
      }),
    ).toEqual({
      packages: [],
      accommodation: { label: 'Single Room (2 nights)', price: '128,00€' },
      total: '128,00€',
      totalWithSponsoring: '128,00€',
    });
  });
  it('should return 100€ for Training Day only', () => {
    expect(
      calculateTicket({
        flatFeeItems: [TRAINING_DAY],
        roomType: SINGLE,
        arrival: JAN_1,
        departure: JAN_1,
      }),
    ).toEqual({
      packages: [{ label: 'Training Day', price: '100,00€' }],
      accommodation: { label: 'Single Room (0 nights)', price: '0,00€' },
      total: '100,00€',
      totalWithSponsoring: '100,00€',
    });
  });
  describe('when sponsoring is less than items price', () => {
    it('should return 276,30€ total for Training Day and SoCraTes, 100€ with 176,30€ of sponsoring', () => {
      expect(
        calculateTicket(
          {
            flatFeeItems: [TRAINING_DAY, SOCRATES],
            roomType: SINGLE,
            arrival: JAN_1,
            departure: JAN_1,
          },
          '176,30€',
        ),
      ).toEqual({
        packages: [
          { label: 'Training Day', price: '100,00€' },
          { label: 'SoCraTes', price: '176,30€' },
        ],
        accommodation: { label: 'Single Room (0 nights)', price: '0,00€' },
        total: '276,30€',
        totalWithSponsoring: '100,00€',
      });
    });
  });
  describe('when sponsoring is more than items price', () => {
    it('should return 276,30€ total for Training Day and SoCraTes, 0€ with 300€ of sponsoring', () => {
      expect(
        calculateTicket(
          {
            flatFeeItems: [TRAINING_DAY, SOCRATES],
            roomType: SINGLE,
            arrival: JAN_1,
            departure: JAN_1,
          },
          '300€',
        ),
      ).toEqual({
        packages: [
          { label: 'Training Day', price: '100,00€' },
          { label: 'SoCraTes', price: '176,30€' },
        ],
        accommodation: { label: 'Single Room (0 nights)', price: '0,00€' },
        total: '276,30€',
        totalWithSponsoring: '0,00€',
      });
    });
  });
});
