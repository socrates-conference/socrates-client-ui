import React from 'react';
import { shallow } from 'enzyme';
import SponsorComponent from './SponsorComponent';
describe('(Component) SponsorRow', () => {
  const wrapper = shallow(<SponsorComponent name="a" logo="b" url="c" />);
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
