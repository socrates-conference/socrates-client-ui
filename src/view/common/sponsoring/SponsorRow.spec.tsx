import React from 'react';
import SponsorRow from './SponsorRow';
import { shallow } from 'enzyme';
describe('(Component) SponsorRow', () => {
  const sponsor = {
    name: 'a',
    logo: 'b',
    url: 'c',
  };
  const wrapper = shallow(<SponsorRow sponsors={[sponsor, sponsor]} />);
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
