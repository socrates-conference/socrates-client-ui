import React from 'react';
import { render, shallow } from 'enzyme';
import Sponsors from './Sponsors';
describe('(Component) Sponsors', () => {
  const title = 'Our Sponsors';
  const sponsors = [
    {
      url: 'http://www.mozaicworks.com',
      logo: 'mozaic_works.png',
      name: 'Mozaic Works',
    },
    {
      url: 'http://www.maibornwolff.de',
      logo: 'maibornWolff.png',
      name: 'MaibornWolff',
    },
    {
      url: 'https://www.seibert-media.net',
      logo: 'seibert.png',
      name: '//SEIBERT/MEDIA GmbH',
    },
  ];
  it('renders without exploding', () => {
    const wrapper = shallow(<Sponsors title={title} sponsors={sponsors} />);
    expect(wrapper).toHaveLength(1);
  });
  it('renders all sponsors', () => {
    const wrapper = render(<Sponsors title={title} sponsors={sponsors} />);
    expect(wrapper.find('.col-lg-6')).toHaveLength(3);
  });
});
