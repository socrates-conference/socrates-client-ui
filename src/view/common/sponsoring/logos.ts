import andrena from '../../../assets/logos/andrena160.png';
import codecentric from '../../../assets/logos/codecentric160.png';
import coderbyheart from '../../../assets/logos/coderbyheart160.png';
import codurance from '../../../assets/logos/codurance160.png';
import commerzbank from '../../../assets/logos/commerzbank160.png';
import connexxo from '../../../assets/logos/connexxo160.png';
import datev from '../../../assets/logos/datev160.png';
import demandflow from '../../../assets/logos/demandflow160.png';
import dpunkt from '../../../assets/logos/dpunkt160.png';
import etermax from '../../../assets/logos/etermax_160.png';
import github from '../../../assets/logos/github160.png';
import holidayCheck from '../../../assets/logos/holidaycheck160.png';
import holisticon from '../../../assets/logos/holisticon160.png';
import huf from '../../../assets/logos/huf160.png';
import improuv from '../../../assets/logos/improuv160.png';
import innoq from '../../../assets/logos/innoq160.png';
import instana from '../../../assets/logos/instana_stan_160.png';
import iso from '../../../assets/logos/iso160.png';
import isys from '../../../assets/logos/isys160.jpg';
import itagile from '../../../assets/logos/it-agile160.png';
import kloseBrothers from '../../../assets/logos/klosebrothers160.png';
import leanovate from '../../../assets/logos/leanovate160.png';
import maibornWolff from '../../../assets/logos/maibornwolff160.png';
import mercateo from '../../../assets/logos/mercateo160.png';
import methodpark from '../../../assets/logos/methodpark160.png';
import mozaicWorks from '../../../assets/logos/mozaic_works160.png';
import msgGillardon from '../../../assets/logos/msgGillardon160.png';
import nordicsemiconductor from '../../../assets/logos/nordicsemiconductor160.png';
import novatec from '../../../assets/logos/novatec160.png';
import oose from '../../../assets/logos/oose160.jpg';
import qixxit from '../../../assets/logos/qixxit160.png';
import rewedigital from '../../../assets/logos/rewedigital160.png';
import seibert from '../../../assets/logos/seibert160.png';
import thinkparq from '../../../assets/logos/thinkparq160.png';
import tng from '../../../assets/logos/tng160.png';
import wikimedia from '../../../assets/logos/wikimedia160.png';
const logos: Record<string, any> = {
  andrena,
  codecentric,
  coderbyheart,
  codurance,
  commerzbank,
  connexxo,
  datev,
  demandflow,
  dpunkt,
  etermax,
  github,
  holidayCheck,
  holisticon,
  huf,
  improuv,
  innoq,
  instana,
  iso,
  isys,
  itagile,
  kloseBrothers,
  leanovate,
  maibornWolff,
  mercateo,
  methodpark,
  mozaic_works: mozaicWorks,
  msgGillardon,
  nordicsemiconductor,
  novatec,
  oose,
  qixxit,
  rewedigital,
  seibert,
  thinkparq,
  tng,
  wikimedia,
};
export default logos;
