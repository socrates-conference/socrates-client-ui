import React from 'react';
import { shallow } from 'enzyme';
import Footer from './Footer';
import StoreFactory, { HistoryFactory } from '../store/store';
import { Provider } from 'react-redux';

function* emptySaga() {}

describe('(Component) Footer', () => {
  const history = HistoryFactory.createTestHistory();
  const store = StoreFactory.createStore({}, emptySaga, history);
  const wrapper = shallow(
    <Provider store={store}>
      <Footer />
    </Provider>,
  );
  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1);
  });
  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
