export type Config = {
  conference: string;
  logoPath: string;
  apiEndpoints: Record<string, string>;
  isRegistrationOpen: boolean;
};
