export function ensure<T>(input: T | undefined | null): T {
  if (input === null || input === undefined)
    throw new Error('Object must not be null');
  return input;
}
