export default function splitArrayIntoTwoParts<T>(
  array: Array<T>,
): Array<Array<T>> {
  const split = Math.ceil(array.length / 2);
  return [array.slice(0, split), array.slice(split)];
}
