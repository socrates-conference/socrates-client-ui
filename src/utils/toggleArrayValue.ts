export const toggleArrayValue = (list: string[], value: string): string[] => {
  const index = list.indexOf(value);
  return index !== -1
    ? [...list.slice(0, index), ...list.slice(index + 1)]
    : [...list, value];
};
