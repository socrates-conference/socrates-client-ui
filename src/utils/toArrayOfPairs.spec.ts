import toArrayOfPairs from './toArrayOfPairs';
describe('toArrayOfPairs', () => {
  it('pairs a single element', () => {
    expect(toArrayOfPairs([1])).toEqual([[1]]);
  });
  it('groups two consecutive elements into single pair', () => {
    expect(toArrayOfPairs([1, 2])).toEqual([[1, 2]]);
  });
  it('returns an empty array for empty array', () => {
    expect(toArrayOfPairs([])).toEqual([]);
  });
  it('forms two pairs out of three elements', () => {
    expect(toArrayOfPairs([1, 2, 3])).toEqual([[1, 2], [3]]);
  });
});
