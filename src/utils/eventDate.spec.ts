import { eventDate } from './eventDate';

describe('eventDate', () => {
  it('renders undefined as "Invalid date"', () => {
    expect(eventDate('2022-08-25', undefined)).toEqual(
      'August 25, 2022 – Invalid date',
    );
  });

  it('start and end date in same month', () => {
    expect(eventDate('2022-08-25', '2022-08-28')).toEqual(
      'August 25 – 28, 2022',
    );
  });

  it('start and end date in different month', () => {
    expect(eventDate('2030-08-30', '2030-09-02')).toEqual(
      'August 30 – September 2, 2030',
    );
  });
});
