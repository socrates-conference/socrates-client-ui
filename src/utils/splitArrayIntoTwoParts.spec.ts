import splitArrayIntoTwoParts from './splitArrayIntoTwoParts';
describe('splitArrayIntoTwoParts', () => {
  it('returns two empty arrays if array is empty', () => {
    const result = splitArrayIntoTwoParts([]);
    expect(result).toHaveLength(2);
    expect(result[0]).toHaveLength(0);
    expect(result[1]).toHaveLength(0);
  });
  it('returns two arrays of equal size if array has even length', () => {
    const array = [1, 2, 3, 4];
    const result = splitArrayIntoTwoParts(array);
    expect(result).toHaveLength(2);
    expect(result[0]).toEqual([1, 2]);
    expect(result[1]).toEqual([3, 4]);
  });
  it('returns two arrays of which first is longer if array has odd length', () => {
    const array = [1, 2, 3, 4, 5];
    const result = splitArrayIntoTwoParts(array);
    expect(result).toHaveLength(2);
    expect(result[0]).toEqual([1, 2, 3]);
    expect(result[1]).toEqual([4, 5]);
  });
});
