import isValidEmailFormat from './isValidEmailFormat';

describe('isValidEmailFormat:', () => {
  it('should recognize a valid email address', () => {
    expect(isValidEmailFormat('test@test.de')).toBe(true);
    expect(isValidEmailFormat('est@TeÄÜÖßt.De')).toBe(true);
  });
  it('should reject an invalid email address', () => {
    expect(isValidEmailFormat('test@test.d')).toBe(false);
    expect(isValidEmailFormat('test')).toBe(false);
    expect(isValidEmailFormat('test@test@de')).toBe(false);
    expect(isValidEmailFormat('test.test.de')).toBe(false);
  });
});
